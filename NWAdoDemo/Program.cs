﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWAdoDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            NWDataContext data = new NWDataContext();
            // data.Log = Console.Out;      // näita SQL päringuid ekraanil
            int loendur = 0;

            // foreach (var x in data.Products) Console.WriteLine(x.ProductName);

            foreach (var cat in data.Categories)
            {
                Console.WriteLine($"Kategooria: {cat.CategoryName}");
                foreach (var prod in data.Products)
                {
                    Console.WriteLine($"\t{prod.ProductName.Trim()}");
                }
            }

            foreach (var p in data.Products)
            {
                // Console.WriteLine($"toode {p.ProductName} kuulub kategooriasse {p.Category.CategoryName}");
            }

            var q = from p in data.Products
                    join c in data.Categories
                    on p.CategoryID equals c.CategoryID
                    orderby c.CategoryName, p.ProductName
                    select new { c.CategoryName, p.ProductID, p.ProductName, p.UnitPrice }
                    ;

            foreach (var x in q) Console.WriteLine($"{++loendur:000}: {x}");
            Console.WriteLine();

            var qs = from p in data.Products
                     group p by p.CategoryID into g
                     join c in data.Categories
                     on g.Key equals c.CategoryID
                     select new
                     {
                         Tootekategooria = c.CategoryName,
                         Laoseis = g.Sum(x => x.UnitsInStock),
                         Bilansis = g.Sum(x => x.UnitPrice * x.UnitsInStock)
                     };

            loendur = 0;
            foreach (var x in qs) Console.WriteLine($"{++loendur:00} - kategooria \"{x.Tootekategooria}\" toodete laoseis on {x.Laoseis} tk bilansilise väärtusega ${x.Bilansis:0}");

        }
    }
}
